const express = require("express");
const router = express.Router();
const authorize = require("_middleware/authorize");
const upload = require("../_middleware/upload");
const excelController = require("./excel.controller");

router.post(
  "/upload",
  authorize(),
  upload.single("file"),
  excelController.upload
);
router.get("/datas", authorize(), excelController.getDatas);

module.exports = router;
