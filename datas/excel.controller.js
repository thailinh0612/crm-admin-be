const db = require("../_helpers/db");
const _ = require("lodash");
const readXlsxFile = require("read-excel-file/node");

const upload = async (req, res) => {
  try {
    if (req.file == undefined) {
      return res.status(400).send("Please upload an excel file!");
    }

    let path =
      __basedir + "/resources/static/assets/uploads/" + req.file.filename;

    readXlsxFile(path).then((rows) => {
      // skip header
      rows.shift();

      let datas = [];

      rows.forEach((row) => {
        let data = {
          so_number: row[1],
          is_visited: row[2],
          date_of_request: row[3],
          time_of_request: row[4],
          customer_name: row[5],
          appointment_date: row[6],
          appointment_time: row[7],
          so_mch_model: row[8],
          address: row[9],
          customer_number: row[10],
          so_serial_number: row[11],
          column2: row[12],
          symptom_symptom_id: row[13],
          service_type_service_id: row[14],
          service_priority_priority_id: row[15],
          service_type_service_status: row[16],
          contract_id: row[17],
          customer_id: row[18],
          revisit: row[19],
          original_sro_id: row[20],
          last_modified_sro_id: row[21],
          assign_datetime: row[22],
          close_datetime: row[23],
          terr_code: row[24],
          so_tech_code_2: row[25],
          service_type_service_id_1: row[26],
          action_description: row[27],
          action_service_visit_type: row[28],
          userId: _.get(req, "user.id", {}),
        };

        datas.push(data);
      });

      db.Data.bulkCreate(datas)
        .then(() => {
          res.status(200).send({
            message: "Uploaded the file successfully: " + req.file.originalname,
          });
        })
        .catch((error) => {
          res.status(500).send({
            message: "Fail to import data into database!",
            error: error.message,
          });
        });
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      message: "Could not upload the file: " + req.file.originalname,
    });
  }
};

const getDatas = (req, res) => {
  const offset =
    (parseInt(_.get(req, "query.page", 1)) - 1) *
    parseInt(_.get(req, "query.size", 1));
  const limit = parseInt(_.get(req, "query.size", 1));

  db.Data.findAndCountAll({
    where: {
      userId: _.get(req, "user.id", 0),
    },
    limit,
    offset,
  })
    .then((data) => {
      res.send({
        data: {
          content: data.rows,
          totalElements: data.count,
        },
      });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials.",
      });
    });
};

module.exports = {
  upload,
  getDatas,
};
