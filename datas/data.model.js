const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  const Data = sequelize.define("Data", {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    so_number: {
      type: DataTypes.STRING,
    },
    is_visited: {
      type: DataTypes.STRING,
    },
    date_of_request: {
      type: DataTypes.STRING,
    },
    time_of_request: {
      type: DataTypes.STRING,
    },
    customer_name: {
      type: DataTypes.STRING,
    },
    appointment_date: {
      type: DataTypes.STRING,
    },
    appointment_time: {
      type: DataTypes.STRING,
    },
    so_mch_model: {
      type: DataTypes.STRING,
    },
    address: {
      type: DataTypes.STRING,
    },
    customer_number: {
      type: DataTypes.STRING,
    },
    so_serial_number: {
      type: DataTypes.STRING,
    },
    column2: {
      type: DataTypes.STRING,
    },
    symptom_symptom_id: {
      type: DataTypes.STRING,
    },
    service_type_service_id: {
      type: DataTypes.STRING,
    },
    service_priority_priority_id: {
      type: DataTypes.STRING,
    },
    service_type_service_status: {
      type: DataTypes.STRING,
    },
    contract_id: {
      type: DataTypes.STRING,
    },
    customer_id: {
      type: DataTypes.STRING,
    },
    revisit: {
      type: DataTypes.STRING,
    },
    original_sro_id: {
      type: DataTypes.STRING,
    },
    last_modified_sro_id: {
      type: DataTypes.STRING,
    },
    assign_datetime: {
      type: DataTypes.DATE,
    },
    close_datetime: {
      type: DataTypes.DATE,
    },
    terr_code: {
      type: DataTypes.STRING,
    },
    so_tech_code_2: {
      type: DataTypes.STRING,
    },
    service_type_service_id_1: {
      type: DataTypes.STRING,
    },
    action_description: {
      type: DataTypes.STRING,
    },
    action_service_visit_type: {
      type: DataTypes.STRING,
    },
    userId: {
      type: DataTypes.STRING,
    },
  });

  return Data;
};
